-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 23, 2017 at 10:44 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_shop`
--
CREATE DATABASE IF NOT EXISTS `db_shop` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `db_shop`;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `adminId` int(11) NOT NULL,
  `adminName` varchar(100) NOT NULL,
  `adminUser` varchar(100) NOT NULL,
  `adminEmail` varchar(100) NOT NULL,
  `adminPass` varchar(32) NOT NULL,
  `level` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`adminId`, `adminName`, `adminUser`, `adminEmail`, `adminPass`, `level`) VALUES
(1, 'Sourav Biswas', 'admin', 'bsripon1@gmail.com', '202cb962ac59075b964b07152d234b70', 0),
(2, 'Ripon Biswas', 'ripon', 'abc@gmail.com', '202cb962ac59075b964b07152d234b70', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_brands`
--

CREATE TABLE `tbl_brands` (
  `brandId` int(11) NOT NULL,
  `brandName` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_brands`
--

INSERT INTO `tbl_brands` (`brandId`, `brandName`) VALUES
(1, 'IPHONE'),
(4, 'SAMSUNG'),
(6, 'ACER'),
(7, 'CANON'),
(8, 'NOKIA'),
(12, 'LENOVO');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cart`
--

CREATE TABLE `tbl_cart` (
  `cartId` int(11) NOT NULL,
  `sId` varchar(255) NOT NULL,
  `productId` int(11) NOT NULL,
  `productName` varchar(255) NOT NULL,
  `price` float(10,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_cart`
--

INSERT INTO `tbl_cart` (`cartId`, `sId`, `productId`, `productName`, `price`, `quantity`, `image`) VALUES
(23, 'ra38ebnbfrb46fti6mutl8n0u5', 10, 'Acer 4G', 6000.00, 1, 'upload/95a9301d64.jpg'),
(24, 'ra38ebnbfrb46fti6mutl8n0u5', 9, 'Iphone 7s', 4500.00, 1, 'upload/14577dab1a.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `catId` int(11) NOT NULL,
  `catName` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`catId`, `catName`) VALUES
(1, 'Desktop'),
(2, 'Laptop'),
(3, 'Mobile Phones'),
(4, 'Accessories'),
(5, 'Software'),
(7, 'Footwear'),
(8, 'Jewellery'),
(9, 'Clothing'),
(10, 'Home Decor &amp; Kitchen'),
(11, 'Beauty &amp; Healthcare'),
(12, 'Toys, Kids &amp; Babies');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_compare`
--

CREATE TABLE `tbl_compare` (
  `id` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `productName` varchar(255) NOT NULL,
  `price` float(10,2) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_customer`
--

CREATE TABLE `tbl_customer` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `zip` varchar(30) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pass` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_customer`
--

INSERT INTO `tbl_customer` (`id`, `name`, `address`, `city`, `country`, `zip`, `phone`, `email`, `pass`) VALUES
(2, 'Sourav Biswas', 'D.C.Road,ctg...', 'Chittagong', 'Bangladesh', '4000', '01817023562', 'one@gmail.com', '202cb962ac59075b964b07152d234b70'),
(3, 'Md. Iqbal', 'pan bahar road', 'Kumilla', 'Bahamas', '6000', '01235692', 'two@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055'),
(4, 'abc', 'nh njj nnnnmn', 'Dhaka', 'Austria', '2300', '0123569850', 'abc@yahoo.com', '827ccb0eea8a706c4c34a16891f84e7b'),
(5, 'abcds', 'bv   hyhyy  ghg', 'Kumilla', 'Armenia', '12350', '0123568950', '123@gmail.com', 'e10adc3949ba59abbe56e057f20f883e'),
(6, 'Kazi ashraf', 'barisal town', 'Kumilla', 'Bangladesh', '6000', '0321313544625', 'abcde@gmail.com', 'dcddb75469b4b4875094e14561e573d8');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order`
--

CREATE TABLE `tbl_order` (
  `id` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `productName` varchar(255) NOT NULL,
  `quantity` tinyint(10) NOT NULL,
  `price` float(10,2) NOT NULL,
  `image` varchar(255) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_order`
--

INSERT INTO `tbl_order` (`id`, `customerId`, `productId`, `productName`, `quantity`, `price`, `image`, `date`, `status`) VALUES
(77, 2, 5, 'Lorem Ipsum is simply', 4, 457.00, 'upload/c9dca426e0.jpg', '2017-08-23 22:14:29', 2),
(78, 2, 9, 'Iphone 7s', 1, 4500.00, 'upload/14577dab1a.png', '2017-08-23 22:27:34', 1),
(79, 2, 7, 'Refregarator', 1, 650.00, 'upload/ab286ca5a7.png', '2017-08-23 22:47:43', 0),
(80, 2, 10, 'Acer 4G', 1, 6000.00, 'upload/95a9301d64.jpg', '2017-08-23 22:55:04', 0),
(81, 2, 6, 'Camera', 1, 450.00, 'upload/08ea63b647.jpg', '2017-08-23 23:07:56', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE `tbl_product` (
  `productId` int(11) NOT NULL,
  `productName` varchar(255) NOT NULL,
  `catId` int(11) NOT NULL,
  `brandId` int(11) NOT NULL,
  `body` text NOT NULL,
  `price` float(10,2) NOT NULL,
  `image` varchar(255) NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`productId`, `productName`, `catId`, `brandId`, `body`, `price`, `image`, `type`) VALUES
(1, 'Lorem Ipsum is simply', 4, 4, '<p>Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..</p>\r\n<p>Product description should be here..Product description should be here..Product description should be here..Product description should be here..</p>\r\n<p>Product description should be here..Product description should be here..</p>\r\n<p>Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..</p>\r\n<p>Product description should be here..Product description should be here..Product description should be here..Product description should be here..</p>\r\n<p>Product description should be here..Product description should be here..</p>\r\n<p>Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..</p>\r\n<p>Product description should be here..Product description should be here..Product description should be here..Product description should be here..</p>\r\n<p>Product description should be here..Product description should be here..</p>\r\n<p>Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..</p>\r\n<p>Product description should be here..Product description should be here..Product description should be here..Product description should be here..</p>\r\n<p>Product description should be here..Product description should be here..</p>\r\n<p>Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..</p>\r\n<p>Product description should be here..Product description should be here..Product description should be here..Product description should be here..</p>\r\n<p>Product description should be here..Product description should be here..</p>\r\n<p>Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..</p>\r\n<p>Product description should be here..Product description should be here..Product description should be here..Product description should be here..</p>\r\n<p>Product description should be here..Product description should be here..</p>', 620.87, 'upload/88e441e587.jpg', 0),
(2, 'Lorem Ipsum is simply', 4, 4, '<p>Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..</p>\r\n<p>Product description should be here..Product description should be here..Product description should be here..Product description should be here..</p>\r\n<p>Product description should be here..Product description should be here..</p>\r\n<p>Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..</p>\r\n<p>Product description should be here..Product description should be here..Product description should be here..Product description should be here..</p>\r\n<p>Product description should be here..Product description should be here..</p>\r\n<p>Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..</p>\r\n<p>Product description should be here..Product description should be here..Product description should be here..Product description should be here..</p>\r\n<p>Product description should be here..Product description should be here..</p>\r\n<p>Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..</p>\r\n<p>Product description should be here..Product description should be here..Product description should be here..Product description should be here..</p>\r\n<p>Product description should be here..Product description should be here..</p>', 403.00, 'upload/6a4276c563.jpg', 0),
(3, ' Lorem Ipsum is simply', 4, 7, '<p>&nbsp;Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..</p>\r\n<p>Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..<br />Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..<br />Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..<br />Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..<br />Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..<br />Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..</p>\r\n<p>Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..</p>\r\n<p>Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..<br />Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..<br />Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..<br />Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..<br />Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..<br />Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..</p>\r\n<p>&nbsp;</p>\r\n<p>Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..</p>\r\n<p>Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..<br />Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..<br />Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..<br />Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..<br />Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..<br />Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..Product description should be here..</p>', 260.00, 'upload/a9afe7f41d.png', 0),
(4, 'Lorem Ipsum is simply', 2, 4, '<p>Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..</p>', 500.00, 'upload/cdc53ec8db.jpg', 1),
(5, 'Lorem Ipsum is simply', 10, 4, '<p>Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..</p>\r\n<p>Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..</p>\r\n<p>Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..</p>\r\n<p>Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..</p>\r\n<p>&nbsp;</p>\r\n<p>Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..</p>\r\n<p>Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..</p>\r\n<p>Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..</p>\r\n<p>Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..</p>\r\n<p>Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..</p>\r\n<p>Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..</p>\r\n<p>Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..</p>\r\n<p>Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..</p>', 457.00, 'upload/c9dca426e0.jpg', 0),
(6, 'Camera', 4, 7, '<p>Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..</p>\r\n<p>&nbsp;</p>\r\n<p>Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..</p>', 450.00, 'upload/08ea63b647.jpg', 1),
(7, 'Refregarator', 10, 4, '<p>Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..</p>\r\n<p>&nbsp;</p>\r\n<p>Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..</p>\r\n<p>Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..Description should be there..</p>', 650.00, 'upload/ab286ca5a7.png', 1),
(8, 'Camera', 4, 7, '<p>Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..</p>\r\n<p>Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..</p>\r\n<p>Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..</p>\r\n<p>Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..</p>\r\n<p>Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..</p>\r\n<p>Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..</p>\r\n<p>Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..</p>\r\n<p>Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..</p>\r\n<p>Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..</p>\r\n<p>Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..</p>\r\n<p>Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..</p>\r\n<p>Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..</p>\r\n<p>Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..</p>\r\n<p>Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..</p>\r\n<p>Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..Description should be here..</p>', 550.00, 'upload/df36a425ad.jpg', 1),
(9, 'Iphone 7s', 3, 1, '<p>Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...</p>\r\n<p>Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...</p>\r\n<p>Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...</p>\r\n<p>Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...</p>\r\n<p>Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...</p>\r\n<p>Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...</p>\r\n<p>Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...</p>\r\n<p>Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...</p>\r\n<p>Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...</p>\r\n<p>Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...</p>\r\n<p>Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...</p>\r\n<p>Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...Latest Iphone description should be here...</p>', 4500.00, 'upload/14577dab1a.png', 0),
(10, 'Acer 4G', 2, 6, '<p>Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..</p>\r\n<p>Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..</p>\r\n<p>Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..</p>\r\n<p>Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..</p>\r\n<p>here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..</p>\r\n<p>Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..</p>\r\n<p>Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..</p>\r\n<p>here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..</p>\r\n<p>Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..</p>\r\n<p>Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..</p>\r\n<p>here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..</p>\r\n<p>Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..</p>\r\n<p>Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..</p>\r\n<p>here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..</p>\r\n<p>Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..</p>\r\n<p>Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..Acer latest laptop update should be here..</p>', 6000.00, 'upload/95a9301d64.jpg', 0),
(11, 'Filter', 10, 4, '<p>sadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsdd</p>\r\n<p>sadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsdd</p>\r\n<p>sadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsdd</p>\r\n<p>sadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsdd</p>\r\n<p>sadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsdd</p>\r\n<p>sadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsdd</p>\r\n<p>sadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsdd</p>\r\n<p>sadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsddsadadsdd</p>', 500.00, 'upload/b779358df3.jpg', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`adminId`);

--
-- Indexes for table `tbl_brands`
--
ALTER TABLE `tbl_brands`
  ADD PRIMARY KEY (`brandId`);

--
-- Indexes for table `tbl_cart`
--
ALTER TABLE `tbl_cart`
  ADD PRIMARY KEY (`cartId`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`catId`);

--
-- Indexes for table `tbl_compare`
--
ALTER TABLE `tbl_compare`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_customer`
--
ALTER TABLE `tbl_customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_order`
--
ALTER TABLE `tbl_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD PRIMARY KEY (`productId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `adminId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_brands`
--
ALTER TABLE `tbl_brands`
  MODIFY `brandId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tbl_cart`
--
ALTER TABLE `tbl_cart`
  MODIFY `cartId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `catId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tbl_compare`
--
ALTER TABLE `tbl_compare`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_customer`
--
ALTER TABLE `tbl_customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_order`
--
ALTER TABLE `tbl_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
--
-- AUTO_INCREMENT for table `tbl_product`
--
ALTER TABLE `tbl_product`
  MODIFY `productId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
