<?php include '../classes/Category.php'?>
<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>
<?php
if(!isset($_GET['catid']) || $_GET['catid'] == NULL){
    echo "<script>window.location = 'catlist.php'</script>";
}else{

    $id = $_GET['catid'];
}
$category = new Category();
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    //$id=$_GET['catid'];
    $catName = $_POST['catName'];
    $updateCat = $category->catUpdate($catName,$id);

}

?>
    <div class="grid_10">
        <div class="box round first grid">
            <h2>Update Category</h2>
            <div class="block copyblock">
                <?php
                if(isset($updateCat)){
                    echo $updateCat;
                }
                ?>
      <?php
         $getCat = $category->getcatById($id);
         if($getCat){

         while ($result = mysqli_fetch_assoc($getCat)){

        ?>

        <form action="" method="post">
                    <table class="form">
                        <tr>
                            <td>
                                <input type="text" name="catName" value="<?php echo $result['catName']?>" class="medium" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="submit" name="submit" Value="Update" />
                            </td>
                        </tr>
                    </table>
                </form>
             <?php } }?>
            </div>
        </div>
    </div>
    <script>


        jQuery(

            function($) {
                $('#message').fadeOut(800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
            }
        )
    </script>
<?php include 'inc/footer.php';?>