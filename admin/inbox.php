﻿<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>
<?php include '../classes/Cart.php';?>
<?php
$ct = new Cart();
$fm = new Format();
?>
<?php
if(isset($_GET['shiftid'])){
    $id = $_GET['shiftid'];
    $time = $_GET['date'];
    $price = $_GET['price'];
    $shiftOrder = $ct->shiftOrderBy($id,$time,$price);
    if($shiftOrder){
        echo "<script>window.location = 'inbox.php'</script>";
    }
}
?>
<?php
if(isset($_GET['delprotid'])){
    $id = $_GET['delprotid'];
    $time = $_GET['date'];
    $price = $_GET['price'];
    $deleteOrder = $ct->deleteOrder($id,$time,$price);
}
?>
<style>
    .data display datatable tr td{text-align: center}
</style>
        <div class="grid_10">
            <div class="box round first grid">
                <h2>Inbox</h2>
                <?php
                if(isset($deleteOrder)){
                    echo $deleteOrder;
                }
                ?>
                <div class="block">        
                    <table class="data display datatable" id="example">
					<thead>
						<tr>
							<th>Product ID</th>
							<th>Order Time</th>
                            <th>Product</th>
                            <th>Quantity</th>
                            <th>Price</th>
                            <th>Customer ID</th>
                            <th>Address</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
                    <?php

                    $getOrder = $ct->getOrderedProduct();
                    if($getOrder){
                        while ($result = mysqli_fetch_assoc($getOrder)){

                    ?>
						<tr class="odd gradeX">
							<td><?php echo $result['id']?></td>
                            <td><?php echo $fm->formatDate($result['date'])?></td>
                            <td><?php echo $result['productName']?></td>
                            <td><?php echo $result['quantity']?></td>
                            <td>$<?php echo $result['price']?></td>
                            <td><?php echo $result['customerId']?></td>
                            <td><a href="customer.php?custId=<?php echo $result['customerId']?>">View Details</a></td>
                            <?php
                            if($result['status']=='0'){?>
                                <td><a href="?shiftid=<?php echo $result['customerId']?> & price=<?php echo $result['price']?> & date=<?php echo $result['date']?>
                                ">Shifted</a> </td>
                           <?php } elseif($result['status']=='1'){?>
                                <td>Pending</td>

                            <?php }else{ ?>
                                <td><a href="?delprotid=<?php echo $result['customerId']?> & price=<?php echo $result['price']?> & date=<?php echo $result['date']?>
                                ">Remove</a></td>

                                <?php }?>
						</tr>
                    <?php } }?>
					</tbody>
				</table>
               </div>
            </div>
        </div>
<script type="text/javascript">
    $(document).ready(function () {
        setupLeftMenu();

        $('.datatable').dataTable();
        setSidebarHeight();
    });
</script>
<script>


    jQuery(

        function($) {
            $('#message').fadeOut(800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
        }
    )
</script>
<?php include 'inc/footer.php';?>
