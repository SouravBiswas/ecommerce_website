﻿<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>
<?php include '../classes/Cart.php';?>
<?php include '../classes/Brand.php';?>
<?php include '../classes/Category.php';?>
<?php include '../classes/Product.php';?>
<?php include '../classes/Customer.php';?>
<?php
$ct = new Cart();
$brand = new Brand();
$cat = new Category();
$pd = new Product();
$customer = new Customer();
$allPendingOrder = $ct->getAllPendingProduct();
$pendingOrdercount = mysqli_num_rows($allPendingOrder);
$allShiftedOrder = $ct->getAllShiftedProduct();
$shiftedOrdercount = mysqli_num_rows($allShiftedOrder);
$getAllBrand = $brand->getAllBrand();
$allBrandCount = mysqli_num_rows($getAllBrand);
$getAllCategory = $cat->getAllCat();
$allCatCount = mysqli_num_rows($getAllCategory);
$getAllProduct = $pd->getAllProductCount();
$allProductCount = mysqli_num_rows($getAllProduct);
$getAllCustomer = $customer->getAllCustomer();
$allCustomerCount = mysqli_num_rows($getAllCustomer);

?>
        <div class="grid_10">
            <div class="box round first grid">
                <h2> Dashbord</h2><br>
                <div class="row">
                    <div class="col-lg-3 col-md-6" style="">
                        <div class="panel panel-primary" style="">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-adn fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><?php echo $pendingOrdercount?></div>
                                        <div>Total Pending Order</div>
                                    </div>
                                </div>
                            </div>
                            <a href="inbox.php">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-road fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><?php echo  $shiftedOrdercount?></div>
                                        <div>Total Shifted Order</div>
                                    </div>
                                </div>
                            </div>
                            <a href="inbox.php">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-yellow">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-bus fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><?php echo $allBrandCount?></div>
                                        <div>Total Brand</div>
                                    </div>
                                </div>
                            </div>
                            <a href="brandlist.php">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-support fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><?php echo $allCatCount?></div>
                                        <div>Total Category</div>
                                    </div>
                                </div>
                            </div>
                            <a href="catlist.php">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-support fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><?php echo $allProductCount?></div>
                                        <div>Total Product</div>
                                    </div>
                                </div>
                            </div>
                            <a href="productlist.php">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-support fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><?php echo $allCustomerCount?></div>
                                        <div>Total Customer Account</div>
                                    </div>
                                </div>
                            </div>
                            <a href="#">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>

                </div>
                <!-- /.row -->

            </div>
        </div>
<?php include 'inc/footer.php';?>