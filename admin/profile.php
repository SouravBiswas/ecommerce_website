<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>
<?php include '../classes/Customer.php'?>
<?php
$admin = new Customer();
$adminId = Session::get('adminId');
$getAdminInfo = $admin->getAdminInfo($adminId);
?>
<center>
    <div class="grid_10">
        <div class="box round first grid">
            <h2>Admin Profile Information</h2>
            <div class="block copyblock">
                <?php
                if ($getAdminInfo){
                while ($result = mysqli_fetch_assoc($getAdminInfo)){

                ?>

                <table class="form" >
                        <tr style="font-size: 20px">
                            <td width="30%">Name</td>
                            <td width="10%">:</td>
                            <td><?php echo $result['adminName']?></td>
                        </tr>
                        <tr style="font-size: 20px">
                            <td width="30%">User Name</td>
                            <td width="10%">:</td>
                            <td><?php echo $result['adminUser']?></td>
                        </tr>
                        <tr style="font-size: 20px">
                            <td width="30%">Email</td>
                            <td width="10%">:</td>
                            <td><?php echo $result['adminEmail']?></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td style="padding-top: 50px"><button><a href="editprofile.php">Update Info</a></button></td>
                        </tr>

                    </table>
            </div>
            <?php } }?>
        </div>
    </div>
</center>
    <script>


        jQuery(

            function($) {
                $('#message').fadeOut(800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
            }
        )
    </script>
<?php include 'inc/footer.php';?>