<?php include 'inc/header.php'?>
<?php
if(isset($_GET['delpro'])){
    $id = $_GET['delpro'];
    $delProduct = $ct->delProductByCart($id);

}

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    //$id=$_GET['catid'];
    $cartId = $_POST['cartId'];
    $quantity = $_POST['quantity'];
    if ($quantity <=0){

        $autodelete = $ct->delProductByCart($cartId);
    }else{

        $updateCart = $ct->updateCartQuantity($quantity,$cartId);

    }


}
?>

<?php
if (!isset($_GET['id'])){

    echo "<meta http-equiv='refresh' content='0;URL=?id=refresh'/>";
}
?>

 <div class="main">
    <div class="content">
    	<div class="cartoption">		
			<div class="cartpage">
                <?php
                if (isset($updateCart)){
                    echo $updateCart;
                }
                if (isset($delProduct)){
                    echo $delProduct;

                }
                ?>
			    	<h2>Your Cart</h2>

						<table class="tblone">
							<tr>
                                <th width="5%">SL</th>
								<th width="30%">Product Name</th>
								<th width="10%">Image</th>
								<th width="15%">Price</th>
								<th width="15%">Quantity</th>
								<th width="15%">Total Price</th>
								<th width="10%">Action</th>
							</tr>

                            <?php
                            $getCart = $ct->getCartProduct();
                            if ($getCart){
                                $i = 0;
                                $sum = 0;
                                $qty = 0;
                                while ($result = mysqli_fetch_assoc($getCart)){
                                    $i++;
                            ?>

							<tr>
                                <td style="padding-top: 5px"><?php echo $i?></td>
								<td style=""><?php echo $result['productName']?></td>
								<td><img style="width: 90%;height: 20%" src="admin/<?php echo $result['image']?>" alt=""/></td>
								<td>$<?php echo $result['price']?></td>
								<td>
									<form action="" method="post">
                                        <input type="hidden" name="cartId" value="<?php echo $result['cartId']?>"/>

                                        <input type="number" name="quantity" value="<?php echo $result['quantity']?>"/>
										<input type="submit" name="submit" value="Update"/>
									</form>
								</td>
								<td>$<?php
                                    $total = $result['price'] * $result['quantity'];
                                    echo $total
                                    ?>
                                </td>
								<td><a onclick="return confirm('Are you sure to delete ?');" href="?delpro=<?php echo $result['cartId']?>">X</a></td>
							</tr>
                                    <?php
                                    $qty = $qty + $result['quantity'];
                                    $sum = $sum + $total
                                    ?>
                                    <?php } }?>

						</table>
                <?php
                $getData = $ct->checkCartData();
                if($getData){

                ?>

						<table style="float:right;text-align:left;" width="40%">
							<tr>
								<th>Sub Total : </th>
								<td>TK. <?php echo $sum?></td>
							</tr>
							<tr>
								<th>VAT : </th>
								<td>10%</td>
							</tr>
                            <?php
                            $grandTotal = (($sum * 10)/100);
                            $finalAmount = $grandTotal + $sum;
                            Session::set("sum",$finalAmount);
                            Session::set("qty",$qty);
                            ?>
							<tr>
								<th>Grand Total :</th>
								<td>TK. <?php echo $finalAmount?> </td>
							</tr>
					   </table>
                <?php }else{

                    echo "<center><span style='color: red;font-weight: bold'> Cart Empty !!!   Please Shop Now  !!</span></center>";
                }


                ?>
					</div>
					<div class="shopping">
						<div class="shopleft">
							<a href="index.php"> <img src="images/shop.png" alt="" /></a>
						</div>
						<div class="shopright">
							<a href="payment.php"> <img src="images/check.png" alt="" /></a>
						</div>
					</div>
    	</div>  	
       <div class="clear"></div>
    </div>
 </div>

    <script>


        jQuery(

            function($) {
                $('#message').fadeOut(800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
            }
        )
    </script>
<?php include 'inc/footer.php'?>