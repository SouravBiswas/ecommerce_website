<?php
$filepath = realpath(dirname(__FILE__));

include_once ($filepath.'/../lib/Database.php');
include_once ($filepath.'/../helpers/Format.php');


class Brand
{
    private $db;
    private $fm;
    public function __construct(){

        $this->db = new Database();
        $this->fm = new Format();

    }

    public function brandInsert($brandName){

        $brandName = $this->fm->validation($brandName);

        $brandName = mysqli_real_escape_string($this->db->link,$brandName);
        if(empty($brandName)){

            $msg = "<span class='error'>Field must not be empty !!</span>";
            return $msg;
        }else{

            $query = "insert into tbl_brands(brandName) VALUES ('$brandName')";
            $brandInsert = $this->db->insert($query);
            if($brandInsert){
                $msg = "<center><span id='message' class='success' style='font-weight: bold;font-size: 15px'>Brand Inserted Successfully!!</span></center>";
                return $msg;
            }else{
                $msg = "<center><span id='message' class='error' style='font-weight: bold;font-size: 15px'>Brand not Inserted !!</span></center>";
                return $msg;
            }
        }

    }
    public function getAllBrand(){

        $query = "select * from tbl_brands order by brandId DESC ";
        $result = $this->db->select($query);

        return $result;
    }

    public function getbrandById($id){

        $query = "select * from tbl_brands where brandId = '$id'";
        $result = $this->db->select($query);

        return $result;
    }


    public function brandUpdate($brandName,$id){

        $brandName = $this->fm->validation($brandName);

        $brandName = mysqli_real_escape_string($this->db->link,$brandName);
        $id = mysqli_real_escape_string($this->db->link,$id);
        if(empty($brandName)){

            $msg = "<span class='error'>Field must not be empty !!</span>";
            return $msg;
        }else{
            $query = "update tbl_brands set brandName='$brandName' where brandId='$id'";
            $result = $this->db->update($query);
            if($result){
                $msg = "<center><span id='message' class='success' style='font-weight: bold;font-size: 15px'>Brand Updated Successfully !!</span></center>";
                return $msg;
            }
            else{
                $msg = "<center><span id='message' class='error' style='font-weight: bold;font-size: 15px'>Brand not Updated !!</span></center>";
                return $msg;
            }


        }
    }


    public function delBrandById($id){

        $query = "delete from tbl_brands where brandId = '$id'";
        $result = $this->db->delete($query);
        if($result){
            $msg = "<center><span id='message' class='success' style='font-weight: bold;font-size: 15px'>Brand Deleted Successfully !!</span></center>";
            return $msg;
        }else{
            $msg = "<center><span id='message' class='error' style='font-weight: bold;font-size: 15px'>Brand not Deleted !!</span></center>";
            return $msg;
        }
    }




}