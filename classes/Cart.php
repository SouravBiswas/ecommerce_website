<?php
$filepath = realpath(dirname(__FILE__));

include_once ($filepath.'/../lib/Database.php');
include_once ($filepath.'/../helpers/Format.php');

class Cart
    {

    private $db;
    private $fm;
    public function __construct(){

        $this->db = new Database();
        $this->fm = new Format();

    }

    public function addToCart($quantity,$id){

        $quantity = $this->fm->validation($quantity);
        $quantity = mysqli_real_escape_string($this->db->link,$quantity);
        $productId = mysqli_real_escape_string($this->db->link,$id);
        $sId       = session_id();

        $query = "select * from tbl_product where productId = '$productId'";
        $result = $this->db->select($query);
        $result = mysqli_fetch_assoc($result);

        $productName = $result['productName'];
        $price = $result['price'];
        $image = $result['image'];

        $cheakQueery = "select * from tbl_cart where productId = '$productId' and sId = '$sId'";
        $getProduct = $this->db->select($cheakQueery);
        if ($getProduct){
            $msg = "<span id='message' class='error' style='font-weight: bold;color:red;font-size: 15px'>This Product Already Added !!</span>";
            return $msg;
        }else {

            $query = "insert into tbl_cart(sId,productId,productName,price,quantity,image) values('$sId','$productId','$productName','$price','$quantity','$image')";
            $cartInsert = $this->db->insert($query);
            if ($cartInsert) {

                header("Location:cart.php");
            } else {
                header("Location:404.php");
            }
        }

    }

    public function getCartProduct(){

        $sId = session_id();
        $query = "select * from tbl_cart where sId = '$sId'";
        $result = $this->db->select($query);
        return $result;

    }
    public function updateCartQuantity($quantity,$cartId){

        $quantity = $this->fm->validation($quantity);

        $quantity = mysqli_real_escape_string($this->db->link,$quantity);
        $cartId = mysqli_real_escape_string($this->db->link,$cartId);

        $query = "update tbl_cart set quantity ='$quantity' where cartId = '$cartId'";
        $result = $this->db->update($query);
        if($result){
            header("Location:cart.php");
        }
        else{
            $msg = "<center><span id='message' class='error' style='font-weight: bold;color: red;font-size: 18px'>Quantity not Updated !!</span></center>";
            return $msg;
        }

    }

    public function delProductByCart($id){

        $id = $this->fm->validation($id);
        $id = mysqli_real_escape_string($this->db->link,$id);
        $query = "delete from tbl_cart where cartId = '$id'";
        $result = $this->db->delete($query);
        if($result){
            header("Location:cart.php");
        }
        else{
            $msg = "<center><span id='message' class='error' style='font-weight: bold;color: red;font-size: 18px'>Product not Deleted !!</span></center>";
            return $msg;
        }

    }

    public function checkCartData(){
        $sId = session_id();

        $query = "select * from tbl_cart where sId = '$sId'";
        $result = $this->db->select($query);
        return $result;
    }

    public function delCustomerCart(){
        $sId = session_id();
        $query = "delete from tbl_cart where sId = '$sId'";
        $this->db->delete($query);
    }

    public function insertOrderProduct($customerId){
        $sId = session_id();

        $query = "select * from tbl_cart where sId = '$sId'";
        $result = $this->db->select($query);
        if ($result){
            while ($value = mysqli_fetch_assoc($result)){
                $productId = $value['productId'];
                $productName = $value['productName'];
                $quantity = $value['quantity'];
                $price = $value['price'] ;
                $image = $value['image'];
                $query = "insert into tbl_order(customerId,productId,productName,quantity,price,image) values('$customerId','$productId','$productName','$quantity','$price','$image')";
                $orderInsert = $this->db->insert($query);


            }
        }
    }
    public function orderDetails($customerId){
        $query = "select * from tbl_order where customerId = '$customerId' ";
        $result = $this->db->select($query);
        return $result;
    }

    public function getOrderDate($customerId){
        $query = "select date from tbl_order where customerId = '$customerId' ";
        $result = $this->db->select($query);
        return $result;
    }

    public function getOrderedProduct(){
        $query = "select * from tbl_order order by date DESC ";
        $result = $this->db->select($query);
        return $result;
    }

    public function shiftOrderBy($id,$time,$price){
        $id = mysqli_real_escape_string($this->db->link,$id);
        $time = mysqli_real_escape_string($this->db->link,$time);
        $price = mysqli_real_escape_string($this->db->link,$price);

        $query = "update tbl_order set status ='1' where customerId = '$id' and date='$time' and price='$price'";
        $result = $this->db->update($query);
        return $result;
    }

    public function deleteOrder($id,$time,$price){
        $id = mysqli_real_escape_string($this->db->link,$id);
        $time = mysqli_real_escape_string($this->db->link,$time);
        $price = mysqli_real_escape_string($this->db->link,$price);
        $query = "delete from tbl_order where customerId = '$id' and date='$time' and price='$price'";
        $result = $this->db->delete($query);
        if($result){
            $msg = "<center><span id='message' class='success' style='font-weight: bold;text-align:center;color: green;font-size: 18px'>Order Deleted Successfully !!</span></center>";
            return $msg;
        }
        else{
            $msg = "<center><span id='message' class='error' style='font-weight: bold;color: red;text-align:center;font-size: 18px'>Order not Deleted !!</span></center>";
            return $msg;
        }
    }

    public function singleOrderedProduct($cmrId){
        $cmrId = mysqli_real_escape_string($this->db->link,$cmrId);
        $query = "select * from tbl_order where customerId='$cmrId' order by date desc ";
        $result = $this->db->select($query);
        return $result;
    }

    public function checkOrderData($cmrId){
        $query = "select * from tbl_order where customerId = '$cmrId'";
        $result = $this->db->select($query);
        return $result;
    }

    public function ProductShiftConfirm($id,$time,$price){
        $id = mysqli_real_escape_string($this->db->link,$id);
        $time = mysqli_real_escape_string($this->db->link,$time);
        $price = mysqli_real_escape_string($this->db->link,$price);
        $query = "update tbl_order set status ='2' where customerId = '$id' and date ='$time' and price='$price'";
        $result = $this->db->update($query);

    }

    public function getAllPendingProduct(){
        $query = "select * from tbl_order where status = '0'";
        $result = $this->db->select($query);
        return $result;
    }
    public function getAllShiftedProduct(){
        $query = "select * from tbl_order where status = '1'";
        $result = $this->db->select($query);
        return $result;
    }
}