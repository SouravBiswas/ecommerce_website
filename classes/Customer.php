<?php
$filepath = realpath(dirname(__FILE__));

include_once ($filepath.'/../lib/Database.php');
include_once ($filepath.'/../helpers/Format.php');

class Customer
{

    private $db;
    private $fm;
    public function __construct(){

        $this->db = new Database();
        $this->fm = new Format();

    }

    public function customerRegistration($data){
        $name = mysqli_real_escape_string($this->db->link, $data['name']);
        $address = mysqli_real_escape_string($this->db->link, $data['address']);
        $city = mysqli_real_escape_string($this->db->link, $data['city']);
        $country = mysqli_real_escape_string($this->db->link, $data['country']);
        $zip = mysqli_real_escape_string($this->db->link, $data['zip']);
        $phone = mysqli_real_escape_string($this->db->link, $data['phone']);
        $email = mysqli_real_escape_string($this->db->link, $data['email']);
        $pass = mysqli_real_escape_string($this->db->link, $data['pass']);
        $pass = md5($pass);
        if ($name == "" || $address == "" || $city == "" || $country == "" || $zip == "" || $phone == "" || $email == "" || $pass == "") {
            $msg = "<span class='error' id='message' style='font-weight: bold;color: red'>Fields must not be empty !!</span>";
            return $msg;
        }
        $mailQuery = "select * from tbl_customer where email = '$email' limit 1";
        $mailChk = $this->db->select($mailQuery);
        if ($mailChk != false){
            $msg = "<span class='error' id='message' style='font-weight: bold;color:red;'>Email already exist !!</span>";
            return $msg;
        }else{
            $query = "insert into tbl_customer(name,address,city,country,zip,phone,email,pass) values('$name','$address','$city','$country','$zip','$phone','$email','$pass')";
            $customerInsert = $this->db->insert($query);
            if ($customerInsert) {
                $msg = "<center><span id='message' class='success' style='font-weight: bold;color:green;text-align:center;font-size: 15px'>Registration Completed!!Cheak your email to varify your account!!</span></center>";
                return $msg;
            } else {
                $msg = "<center><span id='message' class='error' style='font-weight: bold;color:red;text-align:center;font-size: 15px'>Customer Information not Inserted !!</span></center>";
                return $msg;
            }

        }

    }

    public function customerLogin($data){
        $email = mysqli_real_escape_string($this->db->link, $data['email']);
        $pass = mysqli_real_escape_string($this->db->link, md5($data['pass']));
        if(empty($email) || empty($pass)){
            $msg = "<span class='error' id='message' style='font-weight: bold;color: red'>Fields must not be empty !!</span>";
            return $msg;
        }
        $query = "select * from tbl_customer where email = '$email' and pass = '$pass'";
        $result = $this->db->select($query);
        if ($result != false){
            $value = mysqli_fetch_assoc($result);
            Session::set('customerLogin',true);
            Session::set('customerId',$value['id']);
            Session::set('customerName',$value['name']);
            Session::set('customerEmail',$value['email']);
            header("Location:cart.php");
        }else{
            $msg = "<span class='error' id='message' style='font-weight: bold;color: red'>Email or Password incorrect!!</span>";
            return $msg;
        }


    }

    public function getCustomerData($id){

        $query = "select * from tbl_customer where id = '$id'";
        $result = $this->db->select($query);
        return $result;


    }

    public function customerUpdate($data,$customerId){
        $name = mysqli_real_escape_string($this->db->link, $data['name']);
        $address = mysqli_real_escape_string($this->db->link, $data['address']);
        $city = mysqli_real_escape_string($this->db->link, $data['city']);
        $country = mysqli_real_escape_string($this->db->link, $data['country']);
        $zip = mysqli_real_escape_string($this->db->link, $data['zip']);
        $phone = mysqli_real_escape_string($this->db->link, $data['phone']);
        $email = mysqli_real_escape_string($this->db->link, $data['email']);
        if ($name == "" || $address == "" || $city == "" || $country == "" || $zip == "" || $phone == "" || $email == "") {
            $msg = "<span class='error' id='message' style='font-weight: bold;color: red'>Fields must not be empty !!</span>";
            return $msg;
        }
        else{

            $query = "update tbl_customer set name='$name',address='$address',city='$city',country='$country',zip='$zip',phone='$phone',email='$email' where id='$customerId'";
            $customerUpdate = $this->db->update($query);
            if ($customerUpdate) {
                $msg = "<center><span id='message' class='success' style='font-weight: bold;font-size: 15px;color: green'>Your Profile Updated Successfully!!</span></center>";
                return $msg;
            } else {
                $msg = "<center><span id='message' class='error' style='font-weight: bold;font-size: 15px;color: red'>Your Profile not Updated !!</span></center>";
                return $msg;
            }

        }
    }

    public function getAllCustomer(){
        $query = "select * from tbl_customer ";
        $result = $this->db->select($query);
        return $result;

    }

    public function getAdminInfo($adminId){
        $query = "select * from tbl_admin where adminId = '$adminId' ";
        $result = $this->db->select($query);
        return $result;

    }

    public function adminUpdate($data,$adminId){
        $name = mysqli_real_escape_string($this->db->link, $data['adminName']);
        $userName = mysqli_real_escape_string($this->db->link, $data['adminUser']);
        $email = mysqli_real_escape_string($this->db->link, $data['adminEmail']);
        if ($name == "" || $userName == "" || $email == "") {
            $msg = "<span class='error' id='message' style='font-weight: bold;color: red'>Fields must not be empty !!</span>";
            return $msg;
        }
        else{

            $query = "update tbl_admin set adminName='$name',adminUser='$userName',adminEmail='$email' where adminId='$adminId'";
            $adminUpdate = $this->db->update($query);
            if ($adminUpdate) {
                $msg = "<center><span id='message' class='success' style='font-weight: bold;font-size: 15px;color: green'>Your Profile Updated Successfully!!</span></center>";
                return $msg;
            } else {
                $msg = "<center><span id='message' class='error' style='font-weight: bold;font-size: 15px;color: red'>Your Profile not Updated !!</span></center>";
                return $msg;
            }

        }

    }

    public function updateAdminPass($data,$adminId){
        $oldPass = mysqli_real_escape_string($this->db->link, md5($data['oldpass']));
        //var_dump($oldPass);
        $newpass = mysqli_real_escape_string($this->db->link, md5($data['newpass']));
        if ($oldPass == "" || $newpass == "" ) {
            $msg = "<span class='error' id='message' style='font-weight: bold;color: red'>Fields must not be empty !!</span>";
            return $msg;
        }else{
            $query = "select * from tbl_admin where adminId = '$adminId' ";
            $result = $this->db->select($query);
            $result = mysqli_fetch_assoc($result);
            if ($result['adminPass'] == $oldPass) {
                $query = "update tbl_admin set adminpass='$newpass'";
                $adminPassUpdate = $this->db->update($query);
                if ($adminPassUpdate) {
                    $msg = "<center><span id='message' class='success' style='font-weight: bold;font-size: 15px;color: green'>Your Password Updated Successfully!!</span></center>";
                    return $msg;
                } else {
                    $msg = "<center><span id='message' class='error' style='font-weight: bold;font-size: 15px;color: red'>Your Password not Updated !!</span></center>";
                    return $msg;
                }
            } else {
                $msg = "<center><span id='message' class='error' style='font-weight: bold;font-size: 15px;color: red'>Your old Password isn't Match !!</span></center>";
                return $msg;
            }

        }
    }


}