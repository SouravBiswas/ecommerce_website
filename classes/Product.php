<?php
$filepath = realpath(dirname(__FILE__));

include_once ($filepath.'/../lib/Database.php');
include_once ($filepath.'/../helpers/Format.php');

class Product
{
    private $db;
    private $fm;

    public function __construct()
    {

        $this->db = new Database();
        $this->fm = new Format();

    }

    public function productInsert($data, $file)
    {

        $productName = mysqli_real_escape_string($this->db->link, $data['productName']);
        $catId = mysqli_real_escape_string($this->db->link, $data['catId']);
        $brandId = mysqli_real_escape_string($this->db->link, $data['brandId']);
        $body = mysqli_real_escape_string($this->db->link, $data['body']);
        $price = mysqli_real_escape_string($this->db->link, $data['price']);
        $type = mysqli_real_escape_string($this->db->link, $data['type']);

        $permited = array('jpg', 'jpeg', 'png', 'gif');
        $file_name = $file['image']['name'];
        $file_size = $file['image']['size'];
        $file_temp = $file['image']['tmp_name'];

        $div = explode('.', $file_name);
        $file_ext = strtolower(end($div));
        $unique_image = substr(md5(time()), 0, 10) . '.' . $file_ext;
        $uploaded_image = "upload/" . $unique_image;
        if ($productName == "" || $catId == "" || $brandId == "" || $body == "" || $price == "" || $file_name == "" || $type == "") {
            $msg = "<span class='error' style='font-weight: bold'>Fields must not be empty !!</span>";
            return $msg;
        } else {
            move_uploaded_file($file_temp, $uploaded_image);
            $query = "insert into tbl_product(productName,catId,brandId,body,price,image,type) values('$productName','$catId','$brandId','$body','$price','$uploaded_image','$type')";
            $productInsert = $this->db->insert($query);
            if ($productInsert) {
                $msg = "<center><span id='message' class='success' style='font-weight: bold;text-align:center;font-size: 15px'>Product Inserted Successfully!!</span></center>";
                return $msg;
            } else {
                $msg = "<center><span id='message' class='error' style='font-weight: bold;text-align:center;font-size: 15px'>Product not Inserted !!</span></center>";
                return $msg;
            }
        }
    }

    public function getAllProduct()
    {

        $query = "select tbl_product.*,tbl_category.catName,tbl_brands.brandName
                  from tbl_product
                  inner join tbl_category
                  on tbl_product.catId = tbl_category.catId
                  inner join tbl_brands
                  on tbl_product.brandId = tbl_brands.brandId
                  order by tbl_product.productId desc";
        $result = $this->db->select($query);
        return $result;
    }

    public function getProductById($id)
    {
        $query = "select * from tbl_product where productId = '$id'";
        $result = $this->db->select($query);

        return $result;
    }

    public function productUpdate($data, $file, $id)
    {
        $productName = mysqli_real_escape_string($this->db->link, $data['productName']);
        $catId = mysqli_real_escape_string($this->db->link, $data['catId']);
        $brandId = mysqli_real_escape_string($this->db->link, $data['brandId']);
        $body = mysqli_real_escape_string($this->db->link, $data['body']);
        $price = mysqli_real_escape_string($this->db->link, $data['price']);
        $type = mysqli_real_escape_string($this->db->link, $data['type']);

        $permited = array('jpg', 'jpeg', 'png', 'gif');
        $file_name = $file['image']['name'];
        $file_size = $file['image']['size'];
        $file_temp = $file['image']['tmp_name'];

        $div = explode('.', $file_name);
        $file_ext = strtolower(end($div));
        $unique_image = substr(md5(time()), 0, 10) . '.' . $file_ext;
        $uploaded_image = "upload/" . $unique_image;
        if ($productName == "" || $catId == "" || $brandId == "" || $body == "" || $price == "" || $type == "") {
            $msg = "<span class='error' style='font-weight: bold'>Fields must not be empty !!</span>";
            return $msg;
        } else {
            if(!empty($file_name)) {

                if ($file_size > 1048567) {

                    echo "<center><span id='message' class='error' style='font-weight: bold;font-size: 15px'>
            Image size should be less then 1MB!!
            </span>";
                } else {
                    move_uploaded_file($file_temp, $uploaded_image);
                    $query = "update tbl_product set productName='$productName',catId='$catId',brandId='$brandId',body='$body',price='$price',image='$uploaded_image',type='$type' where productId='$id'";
                    $productUpdate = $this->db->update($query);
                    if ($productUpdate) {
                        $msg = "<center><span id='message' class='success' style='font-weight: bold;font-size: 15px'>Product Updated Successfully!!</span></center>";
                        return $msg;
                    } else {
                        $msg = "<center><span id='message' class='error' style='font-weight: bold;font-size: 15px'>Product not Updated !!</span></center>";
                        return $msg;
                    }
                }
            }else{
                $query = "update tbl_product set productName='$productName',catId='$catId',brandId='$brandId',body='$body',price='$price',type='$type' where productId='$id'";
                $productUpdate = $this->db->update($query);
                if ($productUpdate) {
                    $msg = "<center><span id='message' class='success' style='font-weight: bold;font-size: 15px'>Product Updated Successfully!!</span></center>";
                    return $msg;
                } else {
                    $msg = "<center><span id='message' class='error' style='font-weight: bold;font-size: 15px'>Product not Updated !!</span></center>";
                    return $msg;
                }

            }

        }
    }

    public function delProductById($id){

        $query = "select * from tbl_product where productId = '$id'";
        $getDate = $this->db->select($query);
        if ($getDate){
            while ($delimage = mysqli_fetch_assoc($getDate)){
                $deletelink = $delimage['image'];
                unlink($deletelink);
            }
        }
        $delquery = "delete from tbl_product where productId = '$id'";
        $result = $this->db->delete($delquery);
        if($result){
            $msg = "<center><span id='message' class='success' style='font-weight: bold;text-align:center;font-size: 15px'>Product Deleted Successfully !!</span></center>";
            return $msg;
        }else{
            $msg = "<center><span id='message' class='error' style='font-weight: bold;text-align:center;font-size: 15px'>Product not Deleted !!</span></center>";
            return $msg;
        }
    }

    public function getFeaturedProduct(){

        $query = "select * from tbl_product where type = '0' order by productId desc limit 4";
        $result = $this->db->select($query);

        return $result;
    }

    public function getNewProduct(){
        $query = "select * from tbl_product where type = '1' order by productId desc limit 4";
        $result = $this->db->select($query);

        return $result;

    }

    public function getSingleProduct($id){

        $query = "select tbl_product.*,tbl_category.catName,tbl_brands.brandName
                  from tbl_product
                  inner join tbl_category
                  on tbl_product.catId = tbl_category.catId
                  inner join tbl_brands
                  on tbl_product.brandId = tbl_brands.brandId
                  and tbl_product.productId = '$id'";
        $result = $this->db->select($query);
        return $result;

    }

    public function getLatestIphone(){

        $query = "select * from tbl_product where brandId = '1' order by productId desc limit 1";
        $result = $this->db->select($query);

        return $result;

    }

    public function getLatestSamsung(){

        $query = "select * from tbl_product where brandId = '4' order by productId desc limit 1";
        $result = $this->db->select($query);

        return $result;

    }
    public function getLatestAcer(){

        $query = "select * from tbl_product where brandId = '6' order by productId desc limit 1";
        $result = $this->db->select($query);

        return $result;

    }
    public function getLatestCanon(){

        $query = "select * from tbl_product where brandId = '7' order by productId desc limit 1";
        $result = $this->db->select($query);

        return $result;

    }

    public function productByCat($id){
        $catId = mysqli_real_escape_string($this->db->link, $id);

        $query = "select * from tbl_product where catId = '$catId'";
        $result = $this->db->select($query);

        return $result;
    }

    public function insertCompareData($customerId,$cmprId){
        $customerId = mysqli_real_escape_string($this->db->link,$customerId);
        $productId = mysqli_real_escape_string($this->db->link,$cmprId);
        $cheakCompare = "select * from tbl_compare where customerId = '$customerId' and productId = '$productId'";
        $cheak = $this->db->select($cheakCompare);
        if($cheak){
            $msg = "<center><span id='message' class='error' style='font-weight: bold;color:red;text-align:center;font-size: 15px'>Already Added !!</span></center>";
            return $msg;
        }

        $query = "select * from tbl_product where productId = '$productId'";
        $result = $this->db->select($query);
        $result = mysqli_fetch_assoc($result);
        if ($result){
                $productId = $result['productId'];
                $productName = $result['productName'];
                $price = $result['price'];
                $image = $result['image'];
                $query = "insert into tbl_compare(customerId,productId,productName,price,image) values('$customerId','$productId','$productName','$price','$image')";
                $compareInsert = $this->db->insert($query);
            if($compareInsert){
                $msg = "<center><span id='message' class='success' style='font-weight: bold;color:green;text-align:center;font-size: 15px'>Added !! Cheak Compare Page !!</span></center>";
                return $msg;
            }else{
                $msg = "<center><span id='message' class='error' style='font-weight: bold;color:red;text-align:center;font-size: 15px'>Not Added !!</span></center>";
                return $msg;
            }

        }

    }

    public function getComparedProduct($customerId){
        $customerId = mysqli_real_escape_string($this->db->link, $customerId);

        $query = "select * from tbl_compare where customerId = '$customerId' order by id desc";
        $result = $this->db->select($query);

        return $result;
    }

    public function delCompareData($customerId){
        $customerId = mysqli_real_escape_string($this->db->link, $customerId);

        $query = "delete from tbl_compare where customerId = '$customerId'";
        $result = $this->db->delete($query);

    }

    public function getAllProductCount()
    {
        $query = "select * from tbl_product ";
        $result = $this->db->select($query);

        return $result;
    }

}