<?php include 'inc/header.php'?>

 <div class="main">
    <div class="content">
    	<div class="cartoption">		
			<div class="cartpage">
			    	<h2>Product Comparison</h2>

						<table class="tblone">
							<tr>
                                <th>SL</th>
								<th>Product Name</th>
								<th>Price</th>
                                <th>Image</th>
								<th>Action</th>
							</tr>

                            <?php
                            $customerId = Session::get('customerId');
                            $getcomProduct = $pd->getComparedProduct($customerId);
                            if ($getcomProduct){
                                $i = 0;
                                while ($result = mysqli_fetch_assoc($getcomProduct)){
                                    $i++;
                            ?>

							<tr>
                                <td style="padding-top: 5px"><?php echo $i?></td>
								<td style=""><?php echo $result['productName']?></td>
                                <td>$<?php echo $result['price']?></td>
								<td><img style="width: 100px;height: 90px" src="admin/<?php echo $result['image']?>" alt=""/></td>
                                <td><a href="details.php?proid=<?php echo $result['productId']?>">View</a></td>
                            </tr>

                                    <?php } }?>

						</table>
                	</div>
					<div class="shopping">
						<div class="shopleft" style="width: 100%;text-align: center;">
							<a href="index.php"> <img src="images/shop.png" alt="" /></a>
						</div>

					</div>
    	</div>  	
       <div class="clear"></div>
    </div>
 </div>

    <script>


        jQuery(

            function($) {
                $('#message').fadeOut(800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
            }
        )
    </script>
<?php include 'inc/footer.php'?>