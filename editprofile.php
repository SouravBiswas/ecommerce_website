<?php include 'inc/header.php'?>
<?php
$login = Session::get('customerLogin');
if ($login == false){
    header("Location:login.php");
}
?>

<?php
$customerId = Session::get('customerId');
if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit'])){
    $customerUpdate = $cmr->customerUpdate($_POST,$customerId);
}

?>
    <style>
        .tblone{width: 550px;margin: 0 auto;border: 2px solid #ddd;}
        .tblone tr td{text-align: justify}
        .tblone input{width: 286px;padding: 5px;font-size: 15px}
        .tblone select{width: 300px;padding: 5px;font-size: 15px}

    </style>

    <div class="main">
        <div class="content">
            <div class="section group">
                <?php
                $id = Session::get('customerId');
                $getData = $cmr->getCustomerData($id);
                if ($getData){
                    while ($result = mysqli_fetch_assoc($getData)){

                        ?>
                        <h2 style="text-align: center">Update Profile</h2><br>
                        <?php
                        if(isset($customerUpdate)){
                            echo $customerUpdate;
                        }
                        ?>
                <form action="" method="post">
                        <table class="tblone">
                            <tr>
                                <td width="30%">Name</td>
                                <td><input  type="text" name="name" value="<?php echo $result['name']?>"></td>
                            </tr>
                            <tr>
                                <td>Phone</td>
                                <td><input type="text" name="phone" value="<?php echo $result['phone']?>"></td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td><input type="email" name="email" value="<?php echo $result['email']?>"></td>
                            </tr>
                            <tr>
                                <td>Address</td>
                                <td><input type="text" name="address" value="<?php echo $result['address']?>"></td>
                            </tr>
                            <tr>
                                <td>City</td>
                                <td>
                                    <select id="city" name="city">
                                    <option value="null">Select a City</option>
                                    <option value="Dhaka" <?php if(($result['city'])=='Dhaka') echo "selected";?>>Dhaka</option>
                                    <option value="Chittagong" <?php if(($result['city'])=='Chittagong') echo "selected";?>>Chittagong</option>
                                    <option value="Rajshahi" <?php if(($result['city'])=='Rajshahi') echo "selected";?>>Rajshahi</option>
                                    <option value="Khulna" <?php if(($result['city'])=='Khulna') echo "selected";?>>Khulna</option>
                                    <option value="Kumilla" <?php if(($result['city'])=='Kumilla') echo "selected";?>>Kumilla</option>

                                </select>
                                </td>
                            </tr>
                            <tr>
                                <td>ZipCode</td>
                                <td><input type="text" name="zip" value="<?php echo $result['zip']?>"></td>
                            </tr>
                            <tr>
                                <td>Country</td>

                                <td>
                                    <select id="country" name="country">
                                        <option value="null">Select a Country</option>
                                        <option value="Afghanistan" <?php if(($result['country'])=='Afghanistan') echo "selected";?>>Afghanistan</option>
                                        <option value="Armenia" <?php if(($result['country'])=='Armenia') echo "selected";?>>Armenia</option>
                                        <option value="Aruba" <?php if(($result['country'])=='Aruba') echo "selected";?>>Aruba</option>
                                        <option value="Australia" <?php if(($result['country'])=='Australia') echo "selected";?>>Australia</option>
                                        <option value="Austria" <?php if(($result['country'])=='Austria') echo "selected";?>>Austria</option>
                                        <option  value="Bangladesh" <?php if(($result['country'])=='Bangladesh') echo "selected";?>>Bangladesh</option>
                                        <option value="Bahamas" <?php if(($result['country'])=='Bahamas') echo "selected";?>>Bahamas</option>
                                        <option value="Bahrain" <?php if(($result['country'])=='Bahrain') echo "selected";?> >Bahrain</option>

                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><input style="width: 120px;font-size:19px;height: 40px" type="submit" name="submit" value="Update" ></td>
                            </tr>

                        </table>
                </form>
                    <?php } }?>
            </div>
        </div>
    </div>
    <script>


        jQuery(

            function($) {
                $('#message').fadeOut(800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
            }
        )
    </script>
<?php include 'inc/footer.php'?>