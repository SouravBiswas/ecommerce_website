<?php include 'inc/header.php'?>
<?php
$login = Session::get('customerLogin');
if ($login == false){
    header("Location:login.php");
}
?>
<?php
if(isset($_GET['orderid']) && $_GET['orderid']=='order'){
    $customerId = Session::get('customerId');
    $insertOrder = $ct->insertOrderProduct($customerId);
    $delData = $ct->delCustomerCart();
    header("Location:success.php");
}
?>

    <style>
        .division{width: 50%;float: left}
        .tblone{width: 500px;margin: 0 auto;border: 2px solid #ddd;}
        .tblone tr td{text-align: center;}
        .tbltwo{float:right;text-align:left;width: 50%;border: 2px solid #ddd;margin-right: 14px;margin-top: 12px;}
        .tbltwo tr td{text-align: justify;padding: 5px 10px}
        .oredrnow a{width: 180px;margin: 15px auto;text-align: center;font-weight:bold;padding: 5px;font-size: 30px;display: block;background: red;color: white;
        border-radius: 4px}
        .oredrnow{padding-bottom: 30px;}
        .oredrnow a:hover{background-color: lightgrey}
    </style>
    <div class="main">
        <div class="content">
            <div class="section group">
                <div class="division">
                    <table class="tblone">
                        <tr>
                            <th>No</th>
                            <th>Product</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Total Amount</th>
                        </tr>

                        <?php
                        $getCart = $ct->getCartProduct();
                        if ($getCart){
                            $i = 0;
                            $sum = 0;
                            $qty = 0;
                            while ($result = mysqli_fetch_assoc($getCart)){
                                $i++;
                                ?>

                                <tr>
                                    <td style="padding-top: 5px"><?php echo $i?></td>
                                    <td style=""><?php echo $result['productName']?></td>
                                    <td>$<?php echo $result['price']?></td>
                                    <td><?php echo $result['quantity']?></td>
                                    <td>$<?php
                                        $total = $result['price'] * $result['quantity'];
                                        echo $total
                                        ?>
                                    </td>
                                </tr>
                                <?php
                                $qty = $qty + $result['quantity'];
                                $sum = $sum + $total
                                ?>
                            <?php } }?>

                    </table>

                    <table class="tbltwo" >
                        <tr>
                            <td>Sub Total</td>
                            <td>:</td>
                            <td>TK. <?php echo $sum?></td>
                        </tr>
                        <tr>
                            <td>VAT</td>
                            <td>:</td>
                            <td>10%(<?php echo $vat = $sum * .1?>)</td>
                        </tr>
                        <tr>
                            <td>Quantity</td>
                            <td>:</td>
                            <td><?php echo $qty?></td>
                        </tr>
                        <?php
                        $grandTotal = (($sum * 10)/100);
                        $finalAmount = $grandTotal + $sum;
                        ?>
                        <tr>
                            <td>Final Amount</td>
                            <td>:</td>
                            <td>TK. <?php echo $finalAmount?> </td>
                        </tr>
                    </table>
                </div>
                <div class="division">
                    <?php
                    $id = Session::get('customerId');
                    $getData = $cmr->getCustomerData($id);
                    if ($getData){
                        while ($result = mysqli_fetch_assoc($getData)){

                            ?>
                            <table class="tblone"  >
                                <tr>
                                    <td colspan="4"><h2>Your Profile Information</h2></td>
                                </tr>
                                <tr>
                                    <td>Name</td>
                                    <td>:</td>
                                    <td><?php echo $result['name']?></td>
                                </tr>
                                <tr>
                                    <td>Phone</td>
                                    <td>:</td>
                                    <td><?php echo $result['phone']?></td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>:</td>
                                    <td><?php echo $result['email']?></td>
                                </tr>
                                <tr>
                                    <td>Address</td>
                                    <td>:</td>
                                    <td><?php echo $result['address']?></td>
                                </tr>
                                <tr>
                                    <td>City</td>
                                    <td>:</td>
                                    <td><?php echo $result['city']?></td>
                                </tr>
                                <tr>
                                    <td>ZipCode</td>
                                    <td>:</td>
                                    <td><?php echo $result['zip']?></td>
                                </tr>
                                <tr>
                                    <td>Country</td>
                                    <td>:</td>
                                    <td><?php echo $result['country']?></td>
                                </tr>
                                <tr>
                                    <td colspan="3" ><a href="editprofile.php">Update Details</a></td>
                                </tr>

                            </table>
                        <?php } }?>
                </div>
            </div>
        </div>
        <div class="oredrnow"><a href="?orderid=order">Order</a></div>
    </div>
    <script>


        jQuery(

            function($) {
                $('#message').fadeOut(800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
            }
        )
    </script>
<?php include 'inc/footer.php'?>