<?php include 'inc/header.php'?>

<?php
$login = Session::get('customerLogin');
if ($login == false){
    header("Location:login.php");
}
?>
<?php
if(isset($_GET['custid'])){
    $id = $_GET['custid'];
    $time = $_GET['date'];
    $price = $_GET['price'];
    $confirm = $ct->ProductShiftConfirm($id,$time,$price);

}
?>

    <div class="main">
        <div class="content">
            <div class="section group">
                <div class="order">
                    <h2><span>Your Ordered Details</h2>
                    <table class="tblone">
                        <tr>
                            <th>No</th>
                            <th>Product Name</th>
                            <th>Image</th>
                            <th>Quantity</th>
                            <th>Price</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>

                        <?php
                        $cmrId = Session::get('customerId');
                        $getOrder = $ct->singleOrderedProduct($cmrId);
                        if ($getOrder){
                            $i = 0;
                            while ($result = mysqli_fetch_assoc($getOrder)){
                                $i++;
                                ?>

                                <tr>
                                    <td style="padding-top: 5px"><?php echo $i?></td>
                                    <td style=""><?php echo $result['productName']?></td>
                                    <td><img style="width: 10%;height: 20%" src="admin/<?php echo $result['image']?>" alt=""/></td>
                                    <td><?php echo $result['quantity']?></td>

                                    <td>$<?php
                                        $total = $result['price'] ;
                                        echo $total
                                        ?>
                                    </td>
                                    <td><?php echo $fm->formatDate($result['date'])?></td>
                                    <td>
                                        <?php
                                        if ($result['status']=='0'){
                                            echo "Pending";
                                        } elseif($result['status']=='1'){
                                        echo "Shifted";
                                       }else{
                                            echo "OK";
                                        }
                                        ?>
                                    </td>
                                    <?php if ($result['status']=='1'){?>
                                    <td><a href="?custid=<?php echo $cmrId ;?> & price=<?php echo $total?> & date=<?php echo $result['date']?>
                                ">Confirm</a></td>
                                    <?php }elseif($result['status']=='2'){?>
                                    <td>OK</td>
                                    <?php }elseif($result['status']=='0'){?>
                                    <td>N/A</td>
                                    <?php } ?>
                                </tr>

                            <?php } }?>

                    </table>
                </div>

            </div>
            <div class="clear"></div>
        </div>
    </div>
<?php include 'inc/footer.php'?>