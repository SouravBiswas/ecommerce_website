<?php include 'inc/header.php'?>
<?php
$login = Session::get('customerLogin');
if ($login == false){
    header("Location:login.php");
}
?>
    <style>
        .payment{width: 500px;min-width: 200px;height:220px;text-align: center;border: 1px solid #ddd;margin: 0 auto;
        padding: 50px;}
        .payment h2{border-bottom: 1px solid #ddd;margin-bottom: 40px;padding-bottom: 10px;}
        .payment a{background: #ff0000 none repeat scroll 0 0;border-radius: 5px;color: #fff;font-size: 25px;padding: 5px 30px;}
        .payment a:hover{background-color: #9FAFD1}
        .back a{width: 160px;margin: 80px auto 0;padding: 7px 0;
            font-size: 25px;text-align: center;display: block;background: #555;border: 1px solid #333;
        color: #fff;border-radius: 4px;}
        .back a:hover{background-color: #989FA9}
    </style>

    <div class="main">
        <div class="content">
            <div class="section group">
                <div class="payment">
                    <h2>Choose Payment Option</h2>
                    <a href="offline.php">Offline Payment</a>
                    <a href="404.php">Online Payment</a>
                    <div class="back">
                        <a href="cart.php">Previous</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <script>


        jQuery(

            function($) {
                $('#message').fadeOut(800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
            }
        )
    </script>
<?php include 'inc/footer.php'?>