<?php include_once 'lib/Session.php'?>
<?php
Session::init();
include 'lib/Database.php';
include 'helpers/Format.php';
?>

<?php include_once 'classes/Cart.php'?>
<?php include_once 'classes/Customer.php'?>
<?php
$ct = new Cart();
$cmr = new Customer();

$customerId = Session::get('customerId');

$orderDetails = $ct->orderDetails($customerId);

$customerData = $cmr->getCustomerData($customerId);
$orderDate = $ct->getOrderDate($customerId);
$date = mysqli_fetch_assoc($orderDate);
$date = $date['date'];
$customerName = mysqli_fetch_assoc($customerData);
$cusName = $customerName['name'];
$trs="";
$body="";
$srt="";
$grand="";
//$subtotal="";
  if(isset($orderDetails)) {
      $sl=0;
      $sum =0;
       while ($row = mysqli_fetch_assoc($orderDetails)){
          $cusId = $row['customerId'];
          $proId = $row['productId'];
          $proName = $row['productName'];
          $quntity = $row['quantity'];
          $price = $row['price'];
          $total = $price * $quntity;
          $sum = $sum + $total;
          $sl++;
          $trs .= "<tr>";
          $trs .= "<td class=\"service\"> $sl</td>";
          $trs .= "<td class=\"desc\"> $proId </td>";
          $trs .= "<td style='text-align: center' class=\"unit\"> $proName </td>";
          $trs .= "<td class=\"qty\">$$price </td>";
           $trs .= "<td class=\"qty\" > $quntity </td>";
           $trs .= "<td class=\"total\">$$total </td>";
           $trs .= "</tr>";
      }
      $tax = (($sum*10)/100);
      $grandtotal = $sum + $tax;
      $body .= "<tr>";
      $body .= "<td colspan='5'>SUBTOTAL</td>";
      $body .= "<td colspan='5' class=\"total\"> $$sum </td>";
      $body .= "</tr>";

      $srt .= "<tr>";
      $srt .="<td colspan='5'>TAX 10%</td>";
      $srt .= "<td colspan='6' class=\"total\"> $$tax </td>";
      $srt .= "</tr>";

      $grand .= "<tr>";
      $grand .="<td colspan='5'>GRAND TOTAL</td>";
      $grand .= "<td colspan='6' class=\"total\"> $$grandtotal </td>";
      $grand .= "</tr>";
  }

$html= <<<BI

  <head>
    <meta charset="utf-8">
    <title>Invoice</title>
    <link rel="stylesheet" href="admin/css/invoicestyle.css" media="all" />
  </head>
  <body>
    <header class="clearfix">
      <div id="logo">
        
      </div>
      <h1>INVOICE</h1>
      <div id="company" class="clearfix">
        <div>Online Shopping BD,<br /> AZ 85004, Chittagong</div>
        <div>(602) 519-0450</div>
        <div><a href="#">company@example.com</a></div>
      </div>
      <div id="project">
        <div><span>Customer Name</span> $cusName</div>
        <div><span>DATE</span> $date</div>
      </div>
    </header>
    <main>
      <table>
        <thead>
          <tr>
            <th class="service">SL*</th>
            <th class="desc">Product Id</th>
            <th>Product Name</th>
            <th>Price</th>
            <th>QTY</th>
            <th>TOTAL</th>
          </tr>
        </thead>
        <tbody>
          <tr>
          
            $trs
            
          </tr>
          <tr>
            
            $body
            
          </tr>
          <br><br>
          <tr>
            
            $srt
            
          </tr>
          <tr>
            
            $grand
            
          </tr>
        </tbody>
      </table>
      <br>
      <div id="notices">
        <h4 style="text-align: center">Thank You For Purchasing</h4>
      </div>
    </main>
    
  </body>
BI;


// Require composer autoload
require_once ('mpdf/mpdf/mpdf.php');
//Create an instance of the class:

$mpdf = new mPDF();

// Write some HTML code:

$mpdf->WriteHTML(wordwrap($html));

ob_clean();

// Output a PDF file directly to the browser
$mpdf->Output('Invoice.pdf', 'D');


