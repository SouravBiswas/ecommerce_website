<?php include 'inc/header.php'?>
<?php
$login = Session::get('customerLogin');
if ($login == false){
    header("Location:login.php");
}
?>
    <style>
        .tblone{width: 550px;margin: 0 auto;border: 2px solid #ddd;}
        .tblone tr td{text-align: justify}

    </style>

    <div class="main">
       <div class="content">
          <div class="section group">
              <?php
              $id = Session::get('customerId');
              $getData = $cmr->getCustomerData($id);
              if ($getData){
                  while ($result = mysqli_fetch_assoc($getData)){

              ?>
                      <h2 style="text-align: center">Your Profile</h2><br>
               <table class="tblone">
                   <tr>
                       <td width="30%">Name</td>
                       <td width="10%">:</td>
                       <td><?php echo $result['name']?></td>
                   </tr>
                   <tr>
                       <td>Phone</td>
                       <td>:</td>
                       <td><?php echo $result['phone']?></td>
                   </tr>
                   <tr>
                       <td>Email</td>
                       <td>:</td>
                       <td><?php echo $result['email']?></td>
                   </tr>
                   <tr>
                       <td>Address</td>
                       <td>:</td>
                       <td><?php echo $result['address']?></td>
                   </tr>
                   <tr>
                       <td>City</td>
                       <td>:</td>
                       <td><?php echo $result['city']?></td>
                   </tr>
                   <tr>
                       <td>ZipCode</td>
                       <td>:</td>
                       <td><?php echo $result['zip']?></td>
                   </tr>
                   <tr>
                       <td>Country</td>
                       <td>:</td>
                       <td><?php echo $result['country']?></td>
                   </tr>
                   <tr>
                       <td></td>
                       <td></td>
                       <td><a href="editprofile.php">Update Details</a></td>
                   </tr>

               </table>
              <?php } }?>
          </div>
       </div>
    </div>
    <script>


        jQuery(

            function($) {
                $('#message').fadeOut(800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
            }
        )
    </script>
<?php include 'inc/footer.php'?>