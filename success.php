<?php include 'inc/header.php'?>
<?php
$login = Session::get('customerLogin');
if ($login == false){
    header("Location:login.php");
}
?>
    <style>
        .success{width: 500px;min-width: 200px;height:220px;text-align: center;border: 1px solid #ddd;margin: 0 auto;
            padding: 30px;}
        .success h2{border-bottom: 1px solid #ddd;margin-bottom: 20px;padding-bottom: 10px;}
        .success p{line-height: 25px;font-size: 18px;text-align: left;}
        .success button{height: 35px;margin-top: 12px;font-size: 20px}
    </style>

    <div class="main">
        <div class="content">
            <div class="section group">
                <div class="success">
                    <h2>Success</h2>
                    <p>Thanks for Purchase. Receive Your Order Successfully. We will contact you ASAP with delivery details. Here is your order details...
                        <a href="orderdetails.php">Visit Here</a>
                    </p>
                    <h6>OR</h6>
                    <a href="pdf.php"><button type="button" class="btn btn-success"> Download </button></a>
                </div>

            </div>
        </div>
    </div>
    <script>


        jQuery(

            function($) {
                $('#message').fadeOut(800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').flexslider(100);
            }
        )
    </script>
<?php include 'inc/footer.php'?>